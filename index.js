let http = require("http");

let users = [

	{
		username: "moonknight1999",
		email: "moonGod@gmail.com",
		password: "moonKnightStrong"
	},
	{
		username: "kikatMachine",
		email: "notSponsored@gmail.com",
		password: "kitkatForever"
	}

];

let courses = [

	{
		name: "Science 101",
		price: 2500,
		isActive: true
	},
	{
		name: "English 101",
		price: 2500,
		isActive: true
	}

];

http.createServer((req,res)=>{


	console.log(req.url);//request URL endpoint
	console.log(req.method);//request method

	/*
		Not only can we get the request url endpoint to differentiate requests but we can also check for the request method.

		We can think of request methods as the request/client telling our server the action to take for their request.

		This way we don't need to have different endpoints for everything that our client wants to do.

		HTTP methods allow us to group and organize our routes.
		
		With this we can have the same endpoints but with different methods.

		HTTP methods are particularly and primarily concerned with CRUD operations.

		Common HTTP Methods:
		
		GET - Get method for request indicates that the client/request wants to retrieve or get data.

		POST - Post method for request indicates that the client/request wants to post data and create a document.

		PUT - Put method for request indicates that the client/request wants to input data and update a document.

		DELETE - Delete method for request indicates that the client/request wants to delete a document.

	*/

	if(req.url === "/" && req.method === "GET"){

		res.writeHead(200,{'Content-Type': 'text/plain'});
		res.end("Hello World! This route checks for GET Method.")

	} else if(req.url === "/" && req.method === "POST"){

		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end("Hello World! This route checks for POST Method")

	} else if(req.url === "/" && req.method === "PUT"){

		res.writeHead(200,{'Content-Type': 'text/plain'});
		res.end("Hello World! This route checks for PUT Method")

	} else if(req.url === "/" && req.method === "DELETE"){

		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end("Hello World! This route checks for DELETE Method")

	} else if(req.url === "/users" && req.method === "GET"){

		res.writeHead(200,{'Content-Type': 'application/json'});
		res.end(JSON.stringify(users));

	} else if(req.url === "/users" && req.method === "POST"){

		let requestBody = "";

		req.on('data', function(data){

			// console.log(data);
			requestBody += data;
		})

		req.on('end', function(){

			console.log(requestBody);
			requestBody = JSON.parse(requestBody);

			let newUser = {

				username: requestBody.username,
				email: requestBody.email,
				password: requestBody.password
			}

			users.push(newUser);
			console.log(users);

			res.writeHead(200,{'Content-Type': 'application/json'});
			res.end(JSON.stringify(users));
		})

	} else if(req.url === "/courses" && req.method === "GET"){

		res.writeHead(200,{'Content-Type': 'application/json'});
		res.end(JSON.stringify(courses));

	} else if(req.url === "/courses" && req.method === "POST"){

		let requestBody = "";

		req.on('data', function(data){

			requestBody += data;
		})

		req.on('end', function(){

			console.log(requestBody);
			requestBody = JSON.parse(requestBody);

			let newCourse = {

				name: requestBody.name,
				price: requestBody.price,
				isActive: requestBody.isActive
			}

			courses.push(newCourse);
			console.log(courses);

			res.writeHead(200,{'Content-Type': 'application/json'});
			res.end(JSON.stringify(courses));
		})

	}

	/*
		Mini-Activity:

		Create 2 new routes with both GET and POST methods

		First route is on "/users" endpoint and it is a GET method request.
			-Status code: 200
			-Headers: Content-Type: text/plain
			-End the response with end() and send the message:
			-Hello, User! You are getting the details for our users.

		2nd route is on "/users" endpoint and it is a POST method request.
			-Status code: 200
			-Headers: Content-Type: text/plain
			-End the response with end() and send the message:
			-Hello, User! You are creating a new user.

		Create 2 new requests in your postman.
		Send a screenshot of the response for at least 1 of our request.

	*/


}).listen(4000);

console.log('Server is running on localhost:4000')